#ifndef NONTRANSLATABLETABLABEL_H
#define NONTRANSLATABLETABLABEL_H

#include <qt/TabLabel.h>

class NonTranslatableTabLabel : public Ui::TabLabel
{
public:
    NonTranslatableTabLabel(int icon, QString name, char* original, char* context) : Ui::TabLabel(icon, name, original, context) {}

private slots:
    void changeEvent(QEvent*) override {
        // pass because we dont need to translate
    }
};

#endif // NONTRANSLATABLETABLABEL_H
